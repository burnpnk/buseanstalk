import pandas as pd

print('processing excel file')

df = pd.read_excel(
    'DadosBilheticaHF-23jan19.xlsx',
    parse_dates=['DataHoraValidacao']
).rename(
    {
        'DataHoraValidacao': 'datetime',
        'Veiculo': 'bus_id',
        'Carreira': 'route_nr'
    },
    axis=1
).to_pickle(
    '../pickles/ticket_validations.pkl'
)


print('done and saved to ../pickles/ticket_validations.pkl')


