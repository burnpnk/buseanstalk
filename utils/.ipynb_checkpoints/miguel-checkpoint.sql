select 
		router_bus.bus_id,
		router_id,
		trip_start_datetime,
		direction,
		min(datetime),
		max(datetime),
		timestampdiff(MINUTE,min(datetime), max(datetime)),
		max(trip_length),
		sum(seconds_travelled),
		sum(seconds_stopped),
		sum(seconds_travelled) + sum(seconds_stopped),
		bus_gt.stop_name,
		bus_gt.stop_nr
from
	bus_gt
join router_bus
	on router_bus.bus_id = bus_gt.bus_id
group by bus_id, trip_start_datetime
order by trip_start_datetime, datetime, bus_id