import pandas as pd
import numpy as np



def read_bs_data():
    # SELECT * FROM beanstalk.bus_hourly_count WHERE bus_id IN (401, 402, 403, 405, 407, 419);
    bs = pd.read_csv('./data/feto/beanstalk_bus.csv')
    bs['datetime'] = pd.to_datetime(bs.date) + pd.to_timedelta(bs.time)
    bs.drop(['date', 'time'], axis=1, inplace=True)
    bs = bs.set_index(['bus_id', 'datetime']).sort_index()
    return bs


def read_hf_data(groupby=True, carreira_groupby=True, gb_freq='5T'):
    hf = pd.read_excel('./data/ground_truth/DadosBilheticaHF-23jan19.xlsx')
    hf.columns = [col.lower() for col in hf.columns]
    hf = hf.rename({'datahoravalidacao':'datetime', 'veiculo':'bus_id', 'carreira':'route_nr'}, axis=1)
    
    hf.datetime = pd.to_datetime(hf.datetime, format='%Y-%m-%d %H:%M:%S.%f')
    hf.set_index(['datetime'], inplace=True)
    hf['tickets'] = 1
    hf = hf.replace('36A  ', '36A')
    
    # retornar dados em bruto
    if not groupby:
        return hf
    
    # retornar dados em groupby
    grouper = ['bus_id', 'route_nr', pd.Grouper(key='datetime', freq=gb_freq)]\
                if carreira_groupby else ['bus_id', pd.Grouper(key='datetime', freq=gb_freq)]
    return hf.reset_index().groupby(grouper).count()\
                .reset_index().set_index(['bus_id', 'datetime']).sort_index()



def preproc_bus_stops():
    """
        paragens de autocarro
    """
    df_stops = pd.read_csv(
        './data/feto/bus_stops.csv',
        parse_dates=['trip_start_datetime', 'datetime'],
        dtype={'route_nr': str}
    ).drop('bus_id.1', axis=1)

    df_stops = df_stops[(df_stops.trip_start_datetime.map(lambda x: x.year) >= 2018)].set_index(
        'bus_id'
    ).loc[[401, 402, 403, 405, 407, 419]].set_index('trip_start_datetime', append=True).sort_index()
    return df_stops


def preproc_probes():
    """
        probe requests à hora em que são detectados
    """
    df_probes = pd.read_csv(
        './data/feto/bus_probes.csv'
    )

    def rssi_mapper(rssi):
        if rssi < -75:
            return 'wifi'
        elif rssi < -70:
            return 'wifi_75'
        elif rssi < -65:
            return 'wifi_70'
        elif rssi < -60:
            return 'wifi_65'
        elif rssi < -55:
            return 'wifi_60'
        elif rssi < -50:
            return 'wifi_55'
        elif rssi < -45:
            return  'wifi_50'
        else:
            return 'wifi_45'

    dfp = df_probes
    dfp['radius'] = dfp.rssi.map(rssi_mapper)
    dfp.rename({'capture_time':'datetime'}, axis=1, inplace=True)
    dfp.drop('id', axis=1, inplace=True)
    dfp.datetime = pd.to_datetime(dfp.datetime)

    # para fazer o join depois isto deixa de ser preocupante
    dfp['operating_time'] = dfp.datetime.map(lambda time: time.hour not in [2,3,4])
    return dfp


def get_wifi_counts(groupby_freq='15T'):
    """
        contagens de probe requests
    """
    probes = preproc_probes()
    wifi_count = probes.groupby([
        'router_id',
        pd.Grouper(key='datetime', freq=groupby_freq),
        'radius'
    ]).mac_address.count().to_frame().sort_index().unstack().fillna(0).astype(int)
    
    wifi_count.columns = wifi_count.columns.droplevel()
    wifi_count.columns.name = None
    
    wifi_count['sum_50'] = wifi_count[['wifi_45', 'wifi_50']].sum(axis=1)
    wifi_count['sum_55'] = wifi_count[['wifi_45', 'wifi_50', 'wifi_55']].sum(axis=1)
    wifi_count['sum_60'] = wifi_count[[
        'wifi_45', 'wifi_50', 'wifi_55', 'wifi_60'
    ]].sum(axis=1)
    wifi_count['sum_65'] = wifi_count[[
        'wifi_45', 'wifi_50', 'wifi_55', 'wifi_60', 'wifi_65'
    ]].sum(axis=1)
    wifi_count['sum_70'] = wifi_count[[
        'wifi_45', 'wifi_50', 'wifi_55', 'wifi_60', 'wifi_65', 'wifi_70'
    ]].sum(axis=1)
    wifi_count['sum_75'] = wifi_count[[
        'wifi_45', 'wifi_50', 'wifi_55', 'wifi_60', 'wifi_65', 'wifi_70', 'wifi_75'
    ]].sum(axis=1)
    wifi_count['sum'] = wifi_count[[
        i for i in wifi_count.columns if 'wifi' in i
    ]].sum(axis=1)

    return wifi_count


def make_df(freq='5T'):
    bus_stops = preproc_bus_stops().reset_index().set_index('router_id')
    wifi_probes = preproc_probes()
    wifi_counts = get_wifi_counts('5T').reset_index().set_index('router_id')
    #wifi_counts.to_pickle('./wifi_counts.pkl')
    od = pd.read_csv('./data/feto/bus_od.csv', parse_dates=['trip_start_datetime', 'datetime'])
    ground_truth = read_hf_data(gb_freq='5T')
    
    # se fizer isto através de um join dá memory error e eu não estou para me chatear
    router2bus = bus_stops.reset_index().groupby('router_id')['bus_id'].unique().map(lambda x: x[0]).to_frame()
    wifi_counts = wifi_counts.loc[router2bus.index].reset_index()
    wifi_counts['bus_id'] = wifi_counts.router_id.map(lambda x: router2bus.loc[x].values[0])
    
    exits = od.groupby([
        'route_nr',
        pd.Grouper(key='datetime', freq='5T')
    ]).size().to_frame().rename(
        {0: 'exits'}, axis=1
    ).reset_index()
    exits['route_nr'] = exits.route_nr.astype(str)
    
    jn = ground_truth.join(
        wifi_counts.set_index(['bus_id', 'datetime']),
        how='left'
    )
    jn = jn.reset_index()
    jn['route_nr'] = jn['route_nr'].astype(str)

    df = jn.set_index(['route_nr', 'datetime']).sort_index().join(
        exits.set_index(['route_nr', 'datetime']).sort_index(),
        how='left'
    )
    
    return df



""" verificação de datas

# GROUND TRUTH
# INTERESSA TER PELO MENOS ESTAS DATAS
# a linha 2 tem uma data anomala
ground_truth[ground_truth.route_nr != 2].reset_index().datetime.describe()[['first', 'last']].to_frame()

# all good!
bus_stops.datetime.describe()[['first', 'last']]

# igual ao wifi_counts
# a query comeca em 2018-08-01, tendo dado routers a mais
wifi_probes.datetime.describe()[['first', 'last']]

# od, de onde os exits são calculados
od.datetime.describe()[['first', 'last']]

"""