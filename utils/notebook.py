import pandas as pd

import matplotlib.pyplot as plt
plt.style.use('ggplot')
import ipywidgets as wx

from IPython.display import HTML, display
displaydis = lambda html: display(HTML(html))

from tslearn.metrics import dtw
import pylab

from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler, RobustScaler
import numpy as np


def read_hf_data(groupby=True, carreira_groupby=True, gb_freq='5T'):
    hf = pd.read_excel('./data/ground_truth/DadosBilheticaHF-23jan19.xlsx')
    hf.columns = [col.lower() for col in hf.columns]
    hf = hf.rename({'datahoravalidacao':'datetime', 'veiculo':'bus_id'}, axis=1)
    hf.datetime = pd.to_datetime(hf.datetime, format='%Y-%m-%d %H:%M:%S.%f')
    hf.set_index(['datetime'], inplace=True)
    hf['bilhetes'] = 1
    hf = hf.replace('36A  ', '36A')

    # retornar dados em bruto
    if not groupby:
        return hf
    
    # retornar dados em groupby
    grouper = ['bus_id', 'carreira', pd.Grouper(key='datetime', freq=gb_freq)]\
                if carreira_groupby else ['bus_id', pd.Grouper(key='datetime', freq=gb_freq)]
    return hf.reset_index().groupby(grouper).count()\
                .reset_index().set_index(['bus_id', 'datetime']).sort_index()


def read_bs_data():
    # SELECT * FROM beanstalk.bus_hourly_count WHERE bus_id IN (401, 402, 403, 405, 407, 419);
    bs = pd.read_csv('./data/feto/beanstalk_bus.csv')
    bs['datetime'] = pd.to_datetime(bs.date) + pd.to_timedelta(bs.time)
    bs.drop(['date', 'time'], axis=1, inplace=True)
    bs = bs.set_index(['bus_id', 'datetime']).sort_index()
    return bs


def join():
    return read_bs_data().join(
        read_hf_data(), how="right"
    ).reset_index('bus_id').set_index(
        'carreira', append=True
    ).swaplevel().sort_index()


def rmse(df, col1, col2):
    return np.sqrt(mean_squared_error(df[col1], df[col2]))


def dtwe(df, col1, col2):
    return dtw(df[col1], df[col2])


def highlight_min(s, dark_style=False):
    '''
    highlight the minimum in a series
    '''
    is_min = s == s.min()
    stl = '"background-color": "yellow"'
    return [stl if v else '' for v in is_min]


def find_min(sdf, on='rmse', display=True):
    """
    displays min distance between the two series (beanstalk and hf)
    and returns the col name of such min on dtw
    """
    bs_cols = [col for col in sdf.columns if 'count' in col]
    edf = pd.DataFrame(columns=bs_cols, index=['rmse', 'dtw'])
    for col in bs_cols:
        edf[col] = list((rmse(sdf, col, 'bilhetes'), dtwe(sdf, col, 'bilhetes')))
    edf.style.apply(highlight_min, axis=1, dark_style=True)

    if display:
        displaydis(edf.to_html())
    
    try:
        return edf.loc[on].idxmin()
    except:
        print('specify min on either "rmse" or "dtw"')


# widgets
def get_widgets(df):
    wx_selectby = wx.RadioButtons(options=['bus_id', 'carreira', 'both'], value='bus_id', description='select by', disabled=False)

    dfi = (df.groupby(['bus_id', 'carreira']).size().reset_index().drop(0, 1))
    bus2carreiras = {bid: dfi[dfi.bus_id == bid].carreira.unique() for bid in dfi.bus_id.unique()}; del dfi

    wx_bus_id = wx.Dropdown(options=list(bus2carreiras.keys()), description='bus id')
    wx_carreira = wx.Dropdown(options=df.carreira.unique(), disabled=True, description='carreira')

    wx_bs_cols = wx.Dropdown(options=['count'] + ['count_' + str(i) for i in range(40, 80, 5)], description='signal')

    def selectby_observer(*args):
        wx_bus_id.disabled = False if (wx_selectby.value == 'bus_id' or wx_selectby.value == 'both') else True
        wx_carreira.disabled = False if (wx_selectby.value == 'carreira' or wx_selectby.value == 'both') else True

    wx_selectby.observe(selectby_observer, 'value')

    def update_carreira_options(*args):
        wx_carreira.options = tuple(bus2carreiras(wx_bus_id.value))
    #wx_bus_id.observe(update_carreira_options, 'value')
    

def plot(selectby, bus_id, carreira, sinal):
    print(selectby, bus_id, carreira, sinal)
    plt.figure()

def nbinteract():
	wx.interact(plot, selectby = wx_selectby, bus_id = wx_bus_id, carreira = wx_carreira, sinal = wx_bs_cols)


# create new feature based on daytime breakpoints -> 9:00, 12:00, 18:00, later
def hour_classify(timestamp):
    """
    then do
    df.reset_index()
    df['period'] = df.datetime.map(hour_classify)
    #df['period'] = df.datetime.map(lambda dt: dt.hour)
    df = df.set_index(['carreira', 'datetime'])
    """
    hour = timestamp.hour
    if hour <= 9:
        return 1
    elif hour <= 13:
        return 2
    elif hour <= 15:
        return 3
    elif hour <= 18:
        return 4
    elif hour <= 20:
        return 5
    else:
        return 6

    

