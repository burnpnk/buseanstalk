import pandas as pd
import matplotlib.pyplot as plt

from tslearn.metrics import dtw

import numpy as np


MAKE_DUMMIES = True

df = pd.read_pickle('./data/dataset.pkl')
target = ['tickets']
wifi_cols = [col for col in df.columns if 'wifi' in col]

def non_target(dfi):
    return [col for col in dfi.columns if 'tickets' not in col]


def reorder_cols(dfi):
    return dfi[non_target(dfi) + target]


if MAKE_DUMMIES:
    route_dummies = pd.get_dummies(df.route)
    df['hour'] = df.datetime.map(lambda time: time.hour)
    hour_dummies = pd.get_dummies(df.hour, prefix='hour')


def highlight_max(s):
    '''
    highlight the maximum in a Series yellow.
    '''
    is_max = s == s.max()
    return ['background-color: yellow' if v else '' for v in is_max]


def highlight_min(s):
    '''
    highlight the maximum in a Series yellow.
    '''
    is_max = s == s.min()
    return ['background-color: yellow' if v else '' for v in is_max]


def plot_categorical_value_counts_barplots(dropna=True, ret=False):
    
    #print('value counts :: this is based on full, non-missing-data rows')

    flatten = lambda l: [item for sublist in l for item in sublist]
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12.8, 9.6))
    fig.suptitle('data source', y=.93)
    axes = flatten(axes)

    d = df.dropna() if dropna else df
    for eixo, col in zip(axes, ['hour', 'month', 'bus_id', 'route']):
        if col == 'route':
            d.dropna()[col].value_counts(sort=False).plot.bar(ax=eixo);
        else:
            d.dropna()[col].value_counts().sort_index().plot.bar(ax=eixo);    
        eixo.set_xlabel(col)

    if ret:
        return fig


def plot_groupby_count_tickets(dropna=True, ret=False):
    
    #print('groupby [col] ticket counts :: this is based on full,
    # non-missing-data rows')

    flatten = lambda l: [item for sublist in l for item in sublist]
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12.8, 9.6))
    fig.suptitle('ticket validations by feature', y=.93)
    axes = flatten(axes)

    d = df.dropna() if dropna else df
    for eixo, col in zip(axes, ['hour', 'month', 'bus_id', 'route']):
        d.dropna().groupby(col).tickets.sum().plot.bar(ax=eixo)
        eixo.set_xlabel(col)

    if ret:
        return fig


def fun():
    pass

