SELECT
	bus_gt.bus_id,
    router_id,
	trip_nr,
	trip_start_datetime,
	datetime,
	route_nr,
	direction,
	stop_nr,
	stop_order,
    seconds_since_last_stop,
    seconds_stopped,
    seconds_travelled,
    km
FROM
	bus_gt LEFT JOIN router_bus ON bus_gt.bus_id = router_bus.bus_id
WHERE
	bus_gt.bus_id IN (401, 402, 403, 405, 407, 419) AND
	trip_start_datetime BETWEEN "2016-12-31" AND "2019-01-18" AND
    router_bus.router_id >=1000 AND router_bus.router_id <=1020
